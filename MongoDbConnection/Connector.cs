﻿using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDbConnection;

public class Connector
{
	public async Task Connect(string connectionString)
	{
		var client = new MongoClient(connectionString);

		var database = client.GetDatabase("testdb");
		_ = await database.RunCommandAsync((Command<BsonDocument>)"{ping:1}");
	}
}