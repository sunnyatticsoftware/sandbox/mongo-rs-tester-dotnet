# mongo-rs-tester-dotnet

## Requirements
- .NET 7 SDK or runtime
- Docker and docker-compose

## How to run in local
1. Clone the repo
2. Add to `/etc/hosts` (or in Windows `C:\Windows\System32\drivers\etc\hosts`) this entry with the alias for localhost
   ```
   127.0.0.1   mongors
   ```
3. Execute `docker-compose up` to spin up a `mongors` container
4. Run tests with `dotnet test`
5. Verify that the test is successful and it connects to the Mongo Replica Set database

Notice the connection string is `mongodb://mongors:27017,mongors:27018,mongors:27019/?replicaSet=rs0&readPreference=primary&ssl=false`, which uses the alias `mongors`.

## How to run in GitLab CI/CD
Push changes, the `.gitlab-ci.yml` will be triggered. Make sure it's using the desired image version and the desired service alias matching variable `HOST`.