namespace MongoDbConnection.FunctionalTests;

public class ConnectorTests
{
	[Fact]
	public async Task It_Should_Connect_To_Replica_Set_Database()
	{
		var sut = new Connector();
		await sut.Connect("mongodb://mongors:27017,mongors:27018,mongors:27019/?replicaSet=rs0&readPreference=primary&ssl=false");
	}
}